#ifndef MEDIA_FFMPEG_H
#define MEDIA_FFMPEG_H


#ifdef __cplusplus
  extern "C"{
#endif
  #include <libavcodec/avcodec.h>
  #include <libavformat/avformat.h>  
#ifdef __cplusplus
  }
#endif

#endif
