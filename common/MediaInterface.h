#pragma once
#include "Message0.h"
#include "FFmpeg.h"

#ifdef __cplusplus
extern "C" {
#endif
    
   //call when application init
   void media_start();
    
   //call when application destroy
   void media_stop();
    
   //register callback and get notification:
   /*
    register before media_start()
    callback type
    void cb(char* p0, char* p1);
    currently p0 possible value:
    "CONNECTING" "CONNECTED" "DISCONNECTING" "DISCONNECTED"
    "PLAYIING" "BUFFERING" //for audio decode
   */
   int media_cb(void (*cb)(char* , char*));
    
   void mc_reset();
    
   //initiate connection to server
   /*
    media_cb callback will get notified when state change
    onOpen callback will be called if registered in mc_setOnOpen
    test url:
    for broadcaster
    mc_connect("scenecast://192.241.239.217/test?key=123456");
    for viewer
    mc_connect("scenecast://192.241.239.217/test?key=12345678");
   */
   void mc_connect(const char* url);
   //initiate disconnect
   void mc_disconnect();
   /*
       before initiate connection
       set user id
       for test
       mc_setUser("1");
   */
   void mc_setUser(const char* userId);
   /*
       register callback to recv connected, disconnected callback
   */
   void mc_setOnOpen(void (*cb)());
   void mc_setOnClose(void (*cb)());
   //  register callback to recv data message
   //  data includes audio / video / video config
   void mc_setOnMessage(void (*cb)(struct Message0*));
    
   //  for broadcaster to send sps extra info, format is AVCC not annexB
   void mc_setSPSPPS(uint8_t* data, size_t size);
   // for broadcaster to send audio video data, in ffmpeg AVPacket format, H264 & AAC(raw)
   void mc_write(AVPacket* packet);
   
   //decoder setup
   //call when application init
   void adc_init();
    
   //call when application destroy
   void adc_destroy();
    
   //start decoder thread
   void adc_start();
    
   //stop decoder thread
   void adc_stop();
    

   //get decoded data in callback registered in adc_setAudioDecodeCallback
   void adc_adecode(const uint8_t* data, size_t size, int64_t pts); //PCM S16
   //send PCM S16, samples=1024 to encode data
   void adc_aencode(const short* data, int samples, int64_t pts); //PCM S16
   //send video frame data(in AVPacket data format) to decode
   //get called in callback registered in adc_setVideoDecodeCallback
   void adc_vdecode(const uint8_t* data, size_t size, int64_t pts, bool isKeyFrame);
   
   //vtransform transform packet to annex b
   //the user must release the result using adc_release_packet!!
   AVPacket* adc_vtransform(const uint8_t* data, size_t size, int64_t pts, bool isKeyFrame);
    
   //config video parameter, if there is SPSPPS change
   //do this again, internal decoder to reconfig
   //must do it before send frames adc_vdecode
   void adc_setVideoParam(int width, int height, const uint8_t* extradata,  int extradata_size);//set video param
   void adc_setAudioEncodeCallback(void (*cb)(AVPacket*));
   void adc_setAudioDecodeCallback(void (*cb)(AVPacket*));
   void adc_setVideoDecodeCallback(void (*cb)(AVFrame*));
    
    //notification callback
   AVPacket* adc_getAudioDecodeData(int samples);
   
   //release AVPacket get from adc_getAudioDecodeData
   void adc_release_packet(AVPacket* packet);
   void adc_release_frame(AVFrame* frame);
   
#ifdef __cplusplus   
}
#endif