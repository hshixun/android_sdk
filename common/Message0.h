#pragma once

#ifdef __cplusplus
extern "C" {
#endif

struct Message0
{
  const char* type;
  int64_t pts;
  int64_t bPts;
  int isKeyframe;
  int size;
  const uint8_t* data;
};

   
#ifdef __cplusplus   
}
#endif